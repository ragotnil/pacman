#!/bin/bash



function miseAJourCase {
	
	a=$(("$1" - 1))
	b=$(("$2" - 1))
	tput cup "$a" "$b"

	
	info=$(head -c 19 ./dossierMatrice/"$1"/"$2")
	
	if [ "$info" = "1.0.0.0.0.0.0.0.0.0" ]; then
		echo -n "█"
	elif [ "$info" = "0.1.0.0.0.0.0.0.0.0" ]; then
		echo -n "•"
	elif [ "$info" = "0.0.1.0.0.0.0.0.0.0" ]; then
		echo -n -e "\e[33m☺︎\e[0m"
	elif [ "$info" = "0.0.0.1.0.0.0.0.0.0" ]; then
		echo -n -e "\e[33m☺︎\e[0m"
	elif [ "$info" = "0.0.0.0.1.0.0.0.0.0" ]; then
		echo -n -e "\e[33m☺︎\e[0m"
	elif [ "$info" = "0.0.0.0.0.1.0.0.0.0" ]; then
		echo -n -e "\e[33m☺︎\e[0m"
	elif [ "$info" = "0.0.0.0.0.0.1.0.0.0" ]; then
		echo -n -e "\e[31mᗣ\e[0m"
	elif [ "$info" = "0.0.0.0.0.0.0.1.0.0" ]; then
		echo -n -e "\e[34mᗣ\e[0m"
	elif [ "$info" = "0.0.0.0.0.0.0.0.1.0" ]; then
		echo -n -e "\e[35mᗣ\e[0m"
	elif [ "$info" = "0.0.0.0.0.0.0.0.0.1" ]; then
		echo -n -e "\e[32mᗣ\e[0m"
	else
		echo -n " "
	fi
}


function miseAJourPlateau {

	tput civis # faire disparaitre le curseur
	
	coordFile=pileCoord.txt
	n=$(cat "$coordFile" | wc -l )
	#echo "$n"
	for i in $(seq 1 "$n"); do

		coord1=$(head -n 1 "$coordFile" | cut -d '.' -f1)
		coord2=$(head -n 1 "$coordFile" | cut -d '.' -f2)
		miseAJourCase "$coord1" "$coord2"
			
		sed -i '1d' "$coordFile" # suppression de la première ligne du fichier de coord
	done
}

function affichagePlateau {
for l in $(seq 1 31); do
	#printf "%s : " "$l"
	for c in $(seq 1 28); do
		#printf "%s " "$c"
		miseAJourCase "$l" "$c"
	done
	echo -e -n "\n"
	

done
}



#while true; do
	#tput cup 0 0

	#affichagePlateau

	#sleep 0.1

	#tput ed

	#sleep 1
	
	#miseAJourPlateau
#done

#tput cnorm # réaffiche le curseur

