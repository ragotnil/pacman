source "gfx.sh"
source "finDeJeu.sh"
source "utilitaires/entreeUtilisateur.sh"
source "fonctionsPacman.sh"

function boucleJeu {
    boucle=0
    clear
    affichagePlateau

    while [ $boucle -eq 0 ]
    do
        
        # Apelle la fonction qui écrit dans directionPacman.txt
        lireEntreeUtilisateur

        avancerPacman

        #avancerFantome

        miseAJourPlateau

        isFinal=$(final)

        if [ $isFinal -eq 0 ]
        then 
            clear
		echo "je suis finale"
            boucle=42
        fi 
    done
    echo "La partie est finit !"

}
