
function lireEntreeUtilisateur {
    xPacman=$(cat "coordonneesPacman.txt" | cut -d. -f1)
    yPacman=$(cat "coordonneesPacman.txt" | cut -d. -f2)
    #echo "coords : $xPacman.$yPacman"

    echo "$xPacman.$yPacman" >> "pileCoord.txt"

    read -s -t 0.1 -n 1 zqsd
    if test "$zqsd" = 'z'
    then 
        echo "haut" > "directionPacman.txt"
        echo "0.0.1.0.0.0.0.0.0.0" > "dossierMatrice/$xPacman/$yPacman"
    elif  test "$zqsd" = 's'
    then 
        echo "bas" > "directionPacman.txt"
        echo "0.0.0.1.0.0.0.0.0.0" > "dossierMatrice/$xPacman/$yPacman"
    elif  test "$zqsd" = 'q'
    then 
        echo "gauche" > "directionPacman.txt"
        echo "0.0.0.0.0.1.0.0.0.0" > "dossierMatrice/$xPacman/$yPacman"
    elif  test "$zqsd" = 'd'
    then 
        echo "droite" > "directionPacman.txt"
        echo "0.0.0.0.1.0.0.0.0.0" > "dossierMatrice/$xPacman/$yPacman"
    fi
}
