#!/bin/bash

# 2 paramètres : 
# 1er : ligne
# 2ème : colonne
function getInfosCase {
    if [ -z $1 ] || [ -z $2 ]
    then 
        return 0
    fi
    ligne=$1
    colonne=$2
    cheminDossierMatrice=$(cat "$cheminDossierTmp/cheminDossierMatrice.txt")
    cat "$cheminDossierMatrice/$ligne/$colonne"
}

# 2 paramètres : 
# 1er : ligne
# 2ème : colonne
# le contenu doit venir sur l'entrée standard
function setInfosCase {
    if [ -z $1 ] || [ -z $2 ]
    then 
        return 0
    fi
    ligne=$1
    colonne=$2
    read contenu
    cheminDossierMatrice=$(cat "$cheminDossierTmp/cheminDossierMatrice.txt")
    echo "$contenu" > "$cheminDossierMatrice/$ligne/$colonne"
}

