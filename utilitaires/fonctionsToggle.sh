
#Principe des fonctions toggle: 
#elles prennent un parametre en entrée : la string des infos de la case
#leur sortie standard est remplie avec la chaine modifiée (cat)
#on peut donc les utiliser de la manière suivante: 
# getInfosCase | toggle1 | toggle2 | setInfosCase

function toggleGeneral {
    infosCase=$1
    positionParametreAToggle=$2
    etatsDeLaChaine=($(echo $infosCase | cut -d "." -f "1,2,3,4,5,6,7,8,9,10" --output-delimiter " "))
    etatAChanger=${etatsDeLaChaine[positionParametreAToggle]}
    if [ $etatAChanger -eq 0 ]
        then
        nouvelEtat=1
    else 
        nouvelEtat=0
    fi
    etatsDeLaChaine[positionParametreAToggle]=$nouvelEtat
    #On a une liste modifiée des états, il faut maintenant la transformer en string
    chaineDeRetour=""
    i=1
    for etat in ${etatsDeLaChaine[@]}
        do
        if [ ! $i -eq 10 ]
            then
            chaineDeRetour="$chaineDeRetour$etat."
        else 
            chaineDeRetour="$chaineDeRetour$etat"
        fi
        i=$((i + 1))
    done
    echo $chaineDeRetour
}

function togglePiece {
    read infosCase
    toggleGeneral $infosCase 1
}

function togglePacmanHaut {
    read infosCase
    toggleGeneral $infosCase 2
}

function togglePacmanBas {
    read infosCase
    toggleGeneral $infosCase 3
}

function togglePacmanDroite {
    read infosCase
    toggleGeneral $infosCase 4
}

function togglePacmanGauche {
    read infosCase
    toggleGeneral $infosCase 5
}

function toggleFantomeBleu {
    read infosCase
    toggleGeneral $infosCase 6
}

function toggleFantomeOrange {
    read infosCase
    toggleGeneral $infosCase 7
}

function toggleFantomeRose {
    read infosCase
    toggleGeneral $infosCase 8
}

function toggleFantomeRouge {
    read infosCase
    toggleGeneral $infosCase 9
}
