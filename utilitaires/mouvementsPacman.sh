
function mouvementsPossibles {
    lignePacman=$1
    colonnePacman=$2
    nombreLignes=31
    nombreColonnes=28
    chaineDeRetour=""
    #vérification de la case du dessous
    if [ $((lignePacman + 1)) -le $nombreLignes ] 
        then
        #la case existe, il faut vérifier qu'il n'y ait pas de mur dessus
        if [ $( getInfosCase $((lignePacman + 1)) $colonnePacman | cut -d "." -f 1 ) -eq 0 ]
            then
            chaineDeRetour="$chaineDeRetour bas"
        fi
    fi

    #vérification de la case de droite
    if [ $((colonnePacman + 1)) -le $nombreColonnes ] 
        then
        #la case existe, il faut vérifier qu'il n'y ait pas de mur dessus
        if [ $( getInfosCase $lignePacman $((colonnePacman + 1)) | cut -d "." -f 1 ) -eq 0 ]
            then
            chaineDeRetour="$chaineDeRetour droite"
        fi
    fi

    #vérification de la case du haut
    if [ $((lignePacman - 1)) -ge 1 ] 
        then
        #la case existe, il faut vérifier qu'il n'y ait pas de mur dessus
        if [ $( getInfosCase $((lignePacman - 1)) $colonnePacman | cut -d "." -f 1 ) -eq 0 ]
            then
            chaineDeRetour="$chaineDeRetour haut"
        fi
    fi

    #vérification de la case de gauche
    if [ $((colonnePacman - 1)) -ge 1 ] 
        then
        #la case existe, il faut vérifier qu'il n'y ait pas de mur dessus
        if [ $( getInfosCase $lignePacman $((colonnePacman - 1)) | cut -d "." -f 1 ) -eq 0 ]
            then
            chaineDeRetour="$chaineDeRetour gauche"
        fi
    fi

    echo $chaineDeRetour
}