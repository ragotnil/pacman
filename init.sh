#!/bin/bash

#On initialise un dossier dans tmp, puis on crée un fichier qui stockera le dossier jusqu'au dossier matrice dedans
cheminDossierTmp=$(mktemp -d)
touch "$cheminDossierTmp/cheminDossierMatrice.txt"
cheminAbsoluDossierMatrice="$(pwd)/dossierMatrice"
echo $cheminAbsoluDossierMatrice > "$cheminDossierTmp/cheminDossierMatrice.txt"

pieces=$(cat "pieceinit.txt")
for piece in $pieces
do
ligne=$(echo "$piece"|cut -d"." -f1)
colonne=$(echo "$piece"|cut -d"." -f2)
echo "0.1.0.0.0.0.0.0.0.0">"./dossierMatrice/$ligne/$colonne"
done

murs=$(cat "murinit.txt")
for mur in $murs
do
ligne=$(echo "$mur"|cut -d"." -f1)
colonne=$(echo "$mur"|cut -d"." -f2)
echo "1.0.0.0.0.0.0.0.0.0">"./dossierMatrice/$ligne/$colonne"
done

vides=$(cat "videinit.txt")
for vide in $vides
do
ligne=$(echo "$vide"|cut -d"." -f1)
colonne=$(echo "$vide"|cut -d"." -f2)
echo "0.0.0.0.0.0.0.0.0.0">"./dossierMatrice/$ligne/$colonne"
done

echo "0.0.0.0.1.0.0.0.0.0">./dossierMatrice/18/14 #localisation pacman
echo "0.0.0.0.0.0.0.0.0.1">./dossierMatrice/12/10 
echo "0.0.0.0.0.0.1.0.0.0">./dossierMatrice/12/11
echo "0.0.0.0.0.0.0.1.0.0">./dossierMatrice/12/18
echo "0.0.0.0.0.0.0.0.1.0">./dossierMatrice/12/19

echo "18.14">./coordonneesPacman.txt
echo "12.19">./coordonneesfantomeRose.txt
echo "12.18">./coordonneesfantomeOrange.txt
echo "12.11">./coordonneesfantomeBleu.txt
echo "12.10">./coordonneesfantomeRouge.txt
echo "246">./nombrepieces.txt

./menudemarrage.sh

