fichier_coordonnees="coordonneesPacman.txt"

verifTempsPacman(){
        last_update=$(date +%s.%N -r "$fichier_coordonnees")
        current_time=$(date +%s.%N)
        time_diff=$(echo "$current_time - $last_update"| bc)
        if(($(echo "$time_diff <0.5"|bc))); then
                echo "Attendez encore"
                return
        fi
}
